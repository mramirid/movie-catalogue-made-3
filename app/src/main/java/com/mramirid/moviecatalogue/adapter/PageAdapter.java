package com.mramirid.moviecatalogue.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.mramirid.moviecatalogue.tab.ListItemFragment;

public class PageAdapter extends FragmentStatePagerAdapter {

    private int countTab;

    public static final String MOVIES = "movie";
    private static final String TV_SHOWS = "tv";

    public PageAdapter(FragmentManager fm, int countTab) {
        super(fm);
        this.countTab = countTab;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new ListItemFragment(MOVIES);
            case 1:
                return new ListItemFragment(TV_SHOWS);
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return countTab;
    }
}
