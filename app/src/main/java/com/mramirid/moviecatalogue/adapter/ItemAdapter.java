package com.mramirid.moviecatalogue.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.mramirid.moviecatalogue.R;
import com.mramirid.moviecatalogue.model.Item;

import java.util.ArrayList;

public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.ItemViewHolder> {

    private Context context;
    private ArrayList<Item> listItem = new ArrayList<>();
    private OnItemClickCallback onItemClickCallback;

    public void setOnItemClickCallback(OnItemClickCallback onItemClickCallback) {
        this.onItemClickCallback = onItemClickCallback;
    }

    public ItemAdapter(Context context) {
        this.context = context;
    }

    private ArrayList<Item> getListItem() {
        return listItem;
    }

    public void setData(ArrayList<Item> items) {
        listItem.clear();
        listItem.addAll(items);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemRow = LayoutInflater.from(context).inflate(R.layout.item, parent, false);
        return new ItemViewHolder(itemRow);
    }

    @Override
    public void onBindViewHolder(@NonNull final ItemViewHolder holder, final int position) {
        holder.bind(listItem.get(position));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onItemClickCallback.onItemClicked(listItem.get(holder.getAdapterPosition()), holder);
            }
        });
    }

    @Override
    public int getItemCount() {
        return getListItem().size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {

        public ImageView imgPoster;
        TextView tvName, tvYear;
        RatingBar tvRating;

        ItemViewHolder(@NonNull View itemView) {
            super(itemView);

            imgPoster = itemView.findViewById(R.id.img_poster);
            tvName = itemView.findViewById(R.id.tv_name);
            tvRating = itemView.findViewById(R.id.rb_star);
            tvYear = itemView.findViewById(R.id.tv_year);
        }

        void bind(Item item) {
            Glide.with(context).load(item.getPoster()).into(imgPoster);
            tvName.setText(item.getName());
            tvYear.setText(item.getYear());
            tvRating.setRating(item.getRating());
        }
    }

    public interface OnItemClickCallback {
        void onItemClicked(Item item, ItemViewHolder holder);
    }
}