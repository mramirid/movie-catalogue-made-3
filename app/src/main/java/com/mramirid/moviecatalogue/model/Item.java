package com.mramirid.moviecatalogue.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static com.mramirid.moviecatalogue.adapter.PageAdapter.MOVIES;
import static com.mramirid.moviecatalogue.viewmodel.MyViewModel.listGenres;

public class Item implements Parcelable {

    private String poster, name, genres, description, year, language;

    private float rating;

    public Item(JSONObject item, String itemType) {
        try {
            poster = "https://image.tmdb.org/t/p/w342" + item.getString("poster_path");

            name = itemType.equals(MOVIES) ? item.getString("title") : item.getString("name");

            rating = (float) item.getDouble("vote_average") / 2;

            language = item.getString("original_language").toUpperCase();

            year = itemType.equals(MOVIES)
                    ? item.getString("release_date").substring(0, 4)
                    : item.getString("first_air_date").substring(0, 4);

            description = item.getString("overview");

            genres = "";
            JSONArray listIdGenres = item.getJSONArray("genre_ids");
            for (int i = 0; i < listIdGenres.length(); ++i)
                genres += (listGenres.get(listIdGenres.getInt(i)) + ", ");
            genres = genres.substring(0, genres.length() - 2);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getPoster() {
        return poster;
    }

    public String getName() {
        return name;
    }

    public String getGenres() {
        return genres;
    }

    public String getDescription() {
        return description;
    }

    public float getRating() {
        return rating;
    }

    public String getYear() {
        return year;
    }

    public String getLanguage() {
        return language;
    }

    private Item(Parcel in) {
        poster = in.readString();
        name = in.readString();
        genres = in.readString();
        description = in.readString();
        rating = in.readFloat();
        year = in.readString();
        language = in.readString();
    }

    public static final Creator<Item> CREATOR = new Creator<Item>() {
        @Override
        public Item createFromParcel(Parcel in) {
            return new Item(in);
        }

        @Override
        public Item[] newArray(int size) {
            return new Item[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(poster);
        parcel.writeString(name);
        parcel.writeString(genres);
        parcel.writeString(description);
        parcel.writeFloat(rating);
        parcel.writeString(year);
        parcel.writeString(language);
    }
}
