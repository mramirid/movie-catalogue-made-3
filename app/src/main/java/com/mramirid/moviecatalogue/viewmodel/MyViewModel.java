package com.mramirid.moviecatalogue.viewmodel;

import android.annotation.SuppressLint;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.mramirid.moviecatalogue.BuildConfig;
import com.mramirid.moviecatalogue.model.Item;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

import cz.msebera.android.httpclient.Header;

public class MyViewModel extends ViewModel {

	private String itemType;
	private MutableLiveData<ArrayList<Item>> listItem = new MutableLiveData<>();
	private MutableLiveData<Boolean> hasReceived = new MutableLiveData<>();

	@SuppressLint("UseSparseArrays")
	public static HashMap<Integer, String> listGenres = new HashMap<>();

	public void setup(String itemType) {
		this.itemType = itemType;
		String url = "https://api.themoviedb.org/3/genre/" + itemType + "/list?api_key=" + BuildConfig.API_KEY + "&language=en-US";

		AsyncHttpClient client = new AsyncHttpClient();
		client.get(url, new AsyncHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
				try {
					String result = new String(responseBody);
					JSONObject responseObject = new JSONObject(result);
					JSONArray list = responseObject.getJSONArray("genres");

					for (int i = 0; i < list.length(); ++i) {
						JSONObject genre = list.getJSONObject(i);
						int id = genre.getInt("id");
						String genreName = genre.getString("name");
						listGenres.put(id, genreName);
					}

					setItems();
					hasReceived.postValue(true);
				} catch (JSONException e) {
					Log.d("JSONException", Objects.requireNonNull(e.getMessage()));
				}
			}

			@Override
			public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
				Log.d("onFailure", Objects.requireNonNull(error.getMessage()));
				hasReceived.postValue(false);
			}
		});
	}

	private void setItems() {
		AsyncHttpClient client = new AsyncHttpClient();
		final ArrayList<Item> listItemReceived = new ArrayList<>();
		String url = "https://api.themoviedb.org/3/discover/" + itemType + "?api_key=" + BuildConfig.API_KEY + "&language=en-US";

		client.get(url, new AsyncHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
				try {
					String result = new String(responseBody);
					JSONObject responseObject = new JSONObject(result);
					JSONArray list = responseObject.getJSONArray("results");

					for (int i = 0; i < list.length(); ++i) {
						JSONObject itemData = list.getJSONObject(i);
						Item item = new Item(itemData, itemType);
						listItemReceived.add(item);
					}
					listItem.postValue(listItemReceived);
					hasReceived.postValue(true);
				} catch (JSONException e) {
					Log.d("JSONException", Objects.requireNonNull(e.getMessage()));
				}
			}

			@Override
			public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
				Log.d("onFailure", Objects.requireNonNull(error.getMessage()));
				hasReceived.postValue(false);
			}
		});
	}

	public LiveData<ArrayList<Item>> getListItem() {
		return listItem;
	}

	public LiveData<Boolean> getRequestStatus() {
		return hasReceived;
	}
}
