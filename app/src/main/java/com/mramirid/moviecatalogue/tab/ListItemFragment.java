package com.mramirid.moviecatalogue.tab;


import android.app.ActivityOptions;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mramirid.moviecatalogue.R;
import com.mramirid.moviecatalogue.activity.ItemDescriptionActivity;
import com.mramirid.moviecatalogue.adapter.ItemAdapter;
import com.mramirid.moviecatalogue.model.Item;
import com.mramirid.moviecatalogue.viewmodel.MyViewModel;

import java.util.ArrayList;

public class ListItemFragment extends Fragment {

    private String itemType;
    private ItemAdapter itemAdapter;
    private MyViewModel myViewModel;
    private ProgressBar progressBar;

    public ListItemFragment() {
    }

    public ListItemFragment(String itemType) {
        this.itemType = itemType;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_item_list, container, false);

        itemAdapter = new ItemAdapter(getContext());
        itemAdapter.notifyDataSetChanged();

        RecyclerView recyclerView = view.findViewById(R.id.rv_movies);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(itemAdapter);

        int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.default_margin);
        recyclerView.addItemDecoration(new SpacesItemDecoration(spacingInPixels));

        progressBar = view.findViewById(R.id.progress_bar);

        myViewModel = ViewModelProviders.of(this).get(MyViewModel.class);
        myViewModel.getListItem().observe(this, getItem);
        myViewModel.getRequestStatus().observe(this, hasReceived);

        setItems(itemType);

        itemAdapter.setOnItemClickCallback(new ItemAdapter.OnItemClickCallback() {
            @Override
            public void onItemClicked(Item item, ItemAdapter.ItemViewHolder holder) {
                ImageView imageView = holder.imgPoster;
                String transitionName = "posterTransition";
                Intent sharedIntent = new Intent(getActivity(), ItemDescriptionActivity.class);
                sharedIntent.putExtra(ItemDescriptionActivity.KEY_EXTRA, item);
                sharedIntent.putExtra(ItemDescriptionActivity.TRANSITION_EXTRA, transitionName);
                imageView.setTransitionName(transitionName);

                ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(getActivity(), imageView, transitionName);

                startActivity(sharedIntent, options.toBundle());
            }
        });

        return view;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    private void setItems(String itemType) {
        myViewModel.setup(itemType);
        showLoading(true);
    }

    private void showLoading(boolean state) {
        if (state)
            progressBar.setVisibility(View.VISIBLE);
        else
            progressBar.setVisibility(View.GONE);
    }

    private Observer<ArrayList<Item>> getItem = new Observer<ArrayList<Item>>() {
        @Override
        public void onChanged(ArrayList<Item> items) {
            if (items != null) {
                itemAdapter.setData(items);
                showLoading(false);
            }
        }
    };

    private Observer<Boolean> hasReceived = new Observer<Boolean>() {
        @Override
        public void onChanged(Boolean aBoolean) {
            if (!aBoolean) {
                Toast.makeText(getActivity(), R.string.request_failed, Toast.LENGTH_SHORT).show();
                showLoading(false);
            }
        }
    };

    class SpacesItemDecoration extends RecyclerView.ItemDecoration {

        private int space;

        SpacesItemDecoration(int space) {
            this.space = space;
        }

        @Override
        public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
            super.getItemOffsets(outRect, view, parent, state);

            outRect.left = outRect.right = outRect.bottom = space;

            if (parent.getChildLayoutPosition(view) == 0)
                outRect.top = space;
            else
                outRect.top = 0;
        }
    }
}