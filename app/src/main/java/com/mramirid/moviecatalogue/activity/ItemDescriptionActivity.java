package com.mramirid.moviecatalogue.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.mramirid.moviecatalogue.R;
import com.mramirid.moviecatalogue.model.Item;

import java.util.Objects;

public class  ItemDescriptionActivity extends AppCompatActivity {

    public static final String KEY_EXTRA = "key_extra";
    public static final String TRANSITION_EXTRA = "transition_extra";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Objects.requireNonNull(this.getSupportActionBar()).hide();
        setContentView(R.layout.activity_item_description);

        ImageView imgCoverDetail = findViewById(R.id.img_cover_detail);
        ImageView imgPhotoDetail = findViewById(R.id.img_photo_detail);
        TextView tvNameDetail = findViewById(R.id.tv_name_detail);
		RatingBar ratingBar = findViewById(R.id.rb_star);
        TextView tvGenresDetail = findViewById(R.id.tv_genres_detail);
        TextView tvYearDetail = findViewById(R.id.tv_year_detail);
        TextView tvLang = findViewById(R.id.tv_lang_detail);
        TextView tvDescriptionDetail = findViewById(R.id.tv_description_detail);

        Item item = getIntent().getParcelableExtra(KEY_EXTRA);
        String transitionName = getIntent().getStringExtra(TRANSITION_EXTRA);
        imgPhotoDetail.setTransitionName(transitionName);

        if (item != null) {
            Glide.with(this).load(item.getPoster()).centerCrop().into(imgCoverDetail);
            Glide.with(this).load(item.getPoster()).into(imgPhotoDetail);
            tvNameDetail.setText(item.getName());
            ratingBar.setRating(item.getRating());
            tvGenresDetail.setText(item.getGenres());
            tvYearDetail.setText(item.getYear());
			tvLang.setText(item.getLanguage());
            tvDescriptionDetail.setText(item.getDescription());
        }

        ImageButton arrow_back = findViewById(R.id.arrow_back);
        arrow_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}
